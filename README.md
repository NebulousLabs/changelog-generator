# Changelog Generator

A script to avoid changelog merge conflict.

## Usage

### Preparation
* Create `changelog` directory in the root of your repository.
* Copy `generate-changelog.sh` script from `changelog` directory to `changelog/generate-changelog.sh` in your repository.
* Execute `changelog/generate-changelog.sh init` in your repository.
* Edit `changelog-head.md` and `changelog-tail.md` according to your needs.

The script will:
* Download the latest version of the main script to the temp directory.
* Generate directory structure in your repository with directory
  `changelog/items` and three chapter sub-directories `key-updates`,
  `bugs-fixed` and `other`.
* Download template changelog head and tail files if they are not present
  from the previous gangelog generator version.

### Collecting changelog items

Instead of creating new entries directly in `CHANGELOG.MD`, a new file is
created under `items/key-updates`, `items/bugs-fixed` or `items/other`
directory in your repository that documents the change.

Below is an example of how the files are structured by type.

    /changelog
        /items
            /bugs-fixed
                bug1-filename.md
                bug2-filename.md
                bug3-filename.md
                ...
            /key-updates
                update1-filename.md
                update2-filename.md
                ...
            /other
                other1-filename.md
                other2-filename.md
                ...
        changelog-head.md
        changelog-tail.md
        generate-changelog.sh
        README.md

To add a new changelog item, create an `.md` file at the proper location.

### File Format

When naming changelog files, the following format should be used:

```<MR number>-description-string.md```

Example:

```4230-check-contract-gfr.md```

It is important to not use spaces or apostrophes in the filename. In the body of
the file, use markdown to write a detailed description of the issue that will
appear in `CHANGELOG.md`.

Example Body:

```
- Fixed a bug which caused a call to `build.Critical` in the case that a
  contract in the renew set was marked `!GoodForRenew` while the contractor
  lock was not held
```

Multiple changelog items can be entered into one changelog file.

Example body of multiple items in one file `3456-big-change.md`:

```
- Updated module A.
- Refactored module B.
- Created module M.
```

To ensure consistent spacing please remove leading spaces from the first line,
the file content should start with `- ` and please remove new lines and spaces
at the end of the file.

### Ignored Files
Files with filenames listed in `.changelogignore` which are contained
in changelog directory structure will be ignored, i.e. changelog items will
not be created from them. Examples are:

- `.init` files which need to be included if directory structure, so that
otherwise empty directory structure can be committed to git.
- `.DS_Store` files, which are added to directories automatically by MacOS

There is one common `.changelogignore` file defined in Changelog Generator
repository containing `.init` and `.DS_Store` files.

Each repository can contain own specific changelog ignore file at
`changelog/.changelogignore` when needed.

### Change Types
#### Key Updates
Key update are new features and notable product updates. Any key updates should
be added to the version's `key-updates` directory. For new features that require
multiple MRs to complete, only one changelog entry is needed and should be
submitted with the first MR.

#### Bug Fixes
Any bug fixes from the previous releases should be logged under `bugs-fixed`
directory. If bugs are created and fixed in the same release cycle, no changelog
entry is needed.

#### Other
Any other notable changes that users and developers should know about should be
logged under `other` directory. Examples of these would be improves to the build
process, new README files, changes to the CI etc.

### Ordering
Changelog items are sorted in ascending alphabetic order by filenames under
their corresponding section **Key Updates**, **Bugs Fixed**, and **Other** in
the generated changelog. Since the filenames are prefixed with the merge
request number, this means the changes in the changelog will roughly follow
the order of development from oldest to newest.

### Changelog Creation
To create the updated `CHANGELOG.md` file, use the `generate-changelog.sh`
script in the `changelog` directory with a version as an argument.

Example:

```changelog/generate-changelog.sh v1.4.8```

The script creates the changelog by executing the following steps:
- copies `changelog-head.md` to `CHANGELOG.md`
- generates section header for the given version
- generates **Key Updates**, **Bugs Fixed** and **Other** sections
- renders all items in filename alphabetic order under it's specific section in
  `CHANGELOG.md`
- and finally appends `changelog-tail.md`

Once generated, the new `CHANGELOG.md` should be pushed as a new merge request
to be merged with master.

### Updating and Saving Changelog Tail
To create the updated `CHANGELOG.md` file and to save generated version
to `changelog-tail.md` use `final` as the second argument while generating the changelog:

```generate-changelog.sh v1.4.8 final```

The script executes the same steps as without `final` argument and does also:
- saves all active versions to the `changelog-tail.md` in correct order
- deletes all current (i.e. released) items