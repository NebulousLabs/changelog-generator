## This Is Changelog Tail

This tail will be added to the end of the generated CHANGELOG.mg file.
When you execute changelog generator with `final` as a second argument,
the tail will store your released changelog items.

Edit this file as per your needs, e.g. add here your existing changelog
before you start using Changelog Generator.