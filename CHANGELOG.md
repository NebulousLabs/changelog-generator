# Changelog Generator Changelog

## Jun 1, 2020:
### v1.0.1
**Other**
- Move processing CLI arguments from `generate-changelog.sh` to
  `generate-changelog-main.sh` script.

## May 18, 2020:
### v1.0.0
**Key Updates**
- Created initial 1.0.0 version of Changelog Generator
- Use the latest main script in the own repo (not a released script).

