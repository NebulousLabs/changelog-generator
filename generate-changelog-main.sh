#!/usr/bin/env bash
set -e

# Main script to generate changelog from changelog files

# Config

head_filename=changelog-head.md
mid_filename=changelog-mid.md
tail_filename=changelog-tail.md

changelog_md=../CHANGELOG.md
items_dir=items
changelog_ignore=.changelogignore

IFS=$'\n' # fix for loop filenames with spaces

######################################
# Add changelog item to changelog file
# Globals:
#   items_dir
#   changelog_ignore
# Parameters:
#   item sub header, e.g. 'Key Updates'
#   item sub directory, e.g. 'key-updates'
# Outputs:
#   Writes section header to changelog file
#   Writes item to changelog file
######################################
function add_items {
    local items_sub_header="$1"
    local items_sub_dir="$2"
    
    local section_has_items=false
    local items_list=$(find ./"${items_dir}" -wholename "*/${items_sub_dir}/*" | sort)
    local new_line=false

    for item in ${items_list}
    do
        # Skip files from .changelogignore (e.g. .init to be able to commit
        # empty directory structure to git and .DS_Store from MacOS)

        # Repo specific changelog ignore
        # Defined in repo with changelog
        if [[ -f "${changelog_ignore}" ]]; then
            for file_to_ignore in $(cat "${changelog_ignore}")
            do
                if [ "${file_to_ignore}" == $(basename "${item}") ]
                then
                    continue 2
                fi
            done
        fi

        # Common changelog ignore
        # Defined in common changelog generator repo
        pushd $(dirname "$0") > /dev/null
        if [[ -f "${changelog_ignore}" ]]; then
            for file_to_ignore in $(cat "${changelog_ignore}")
            do
                if [ "${file_to_ignore}" == $(basename "${item}") ]
                then
                    popd > /dev/null
                    continue 2
                fi
            done
        fi
        popd > /dev/null

        if [ "${section_has_items}" == false ]
    	then
            # Write section header
    	    section_has_items=true
    		echo "**${items_sub_header}**" >> "${mid_filename}"
    	fi
        # remove trailing new lines from items
        # to fix markdown rendering
        text="$(printf "%s" "$(< ${item})")"

        echo "${text}" >> "${mid_filename}"

        # Remove item (file) when called with final argument
        if [ "$final" == "true" ]; then
            rm -f ${item}
        fi
    done

    # add new line to fix markdown rendering
    if [ "${section_has_items}" == true ]
    then
        echo "" >> "${mid_filename}"
    fi
}

######################################
# Download common changelog ignore file to temp dir
# Globals:
#   main_url
#   changelog_ignore
######################################
function download_changelogignore {
    local url_begin=$(dirname ${main_url})
    local url=${url_begin}/${changelog_ignore}

    pushd $(dirname "$0") > /dev/null
    curl --show-error --fail -o ${changelog_ignore} ${url}
    popd > /dev/null
}

######################################
# Copy common changelog ignore file to temp dir for local execution
# Globals:
#   changelog_ignore
######################################
function copy_changelogignore {
    pushd $(dirname "$0") > /dev/null
    cp "${changelog_ignore}" "${temp_dir}"
    popd > /dev/null
}

######################################
# Init changelog in your git repository
# Globals:
#   main_url
# Output:
#   Creates empty directory structure with items, 3 chapter directories and
#   empty init files (to commit empty directories to git).
#   Downloads template changelog head and tail files if they are not yet
#   present.
######################################
function init {
    # Check that the calling script is inside 'changelog' directory
    local parent_dir="$(basename "$(pwd)")"
    if [[ "${parent_dir}" != "changelog" ]]
    then
        echo "Error:"
        echo "    Please follow the Readme instructions."
        echo "    generate-changelog.sh script should be under 'changelog' directory"
        echo "    but is present at $(pwd)"

        exit 1
    fi

    # Generate directory structure
    # Use init files so that empty directories can be committed to git
    local dirs=("key-updates" "bugs-fixed" "other")
    for dir in ${dirs[@]}
    do
        local item_dir="${items_dir}/${dir}"
        mkdir -p "${item_dir}"
        touch "${item_dir}/.init"
    done

    # Download template head and tail files
    local templates=("changelog-head.md" "changelog-tail.md")
    local url_begin=$(dirname ${main_url})
    for file in ${templates[@]}
    do
        # Do not overwrite if the file already exists
        if [[ -f "${file}" ]]
        then
            continue
        fi

        # Download
        local url="${url_begin}/template-files/${file}"
        curl --show-error --fail -o ${file} ${url}
    done

    echo "Changelog init done."
    echo "Edit your changelog-head.md and changelog-tail.md files."
}

# Check arguments
if [[ $# -eq 0 ]]; then
    echo 'Usage:'
    echo './generate-changelog.sh {init|<version> [final]}'
    echo
    echo 'Arguments:'
    echo 'init or <version>: REQUIRED'
    echo 'init:              Prepares directory structure for storing changelog items.'
    echo '<version>:         Version that the changelog is being generated for.'
    echo 'final OPTIONAL:    If the final flag is used the changelog entries will be deleted and the tail file will be updated.'
    echo
    echo 'Examples:'
    echo './generate-changelog.sh init'
    echo './generate-changelog.sh v1.4.9'
    echo './generate-changelog.sh v1.4.9 final'
    exit 1
fi

# Set variables from arguments
if [[ $1 == init ]]; then
    init=true
else
    version="$1"
fi
final=false # when true: updates tail, removes items
if [[ $2 == final ]]; then
    final=true
fi

# Init changelog and exit
if [ "${init}" == "true" ]
then
    init
    exit 0
fi

# Write the head of the changelog
echo 'writing head of changelog.md'
cp "${head_filename}" "${changelog_md}"

# Create temp changelog-mid.md
touch "${mid_filename}"

# Write version

echo "writing version: ${version}"

# Echo current date month (in English), day and year in format:
# '## Mar 30, 2020:'
echo "## $(LC_ALL=C date +%b) $(date +%-d), $(date +%Y):" > "${mid_filename}"
echo "### ${version}" >> "${mid_filename}"

if [ "${local_execution}" == "true" ]
then
    mkdir -p "${temp_dir}"
    copy_changelogignore
else
    download_changelogignore
fi

add_items "Key Updates" "key-updates"
add_items "Bugs Fixed" "bugs-fixed"
add_items "Other" "other"

# Write mid to tal depending on final argument
if [ "${final}" == "true" ]
then
    echo "" >> "${changelog_md}"

    # Save changelog mid to changelog tail

    # Append the tail of the changelog to the mid
    cat "${tail_filename}" >> "${mid_filename}"

    # Update tail with mid
    echo 'updating tail of changelog'
    mv "${mid_filename}" "${tail_filename}"
else
    echo "" >> "${changelog_md}"

    # Write the mid of the changelog
    echo 'writing mid of changelog.md'
    cat "${mid_filename}" >> "${changelog_md}"
    rm "${mid_filename}"
fi

# Write the tail of the changelog
echo 'writing tail of changelog.md'
cat "$tail_filename" >> "$changelog_md"